#include <iostream>
#include "UnorderedArray.h"
using namespace std;

int main()
{
	int elementSize;
	cout << "Enter size for element sets: ";
	cin >> elementSize;
	UnorderedArray<int> stack(elementSize);
	UnorderedArray<int> queue(elementSize);

	while (true)
	{
		int userOptionInput = 0;
		cout << "What do you want to do?" << endl;
		cout << "1 - Push elements" << endl;
		cout << "2 - Pop elements" << endl;
		cout << "3 - Print everything then empty set" << endl;
		cin >> userOptionInput;
		if (userOptionInput == 1)
		{
			int userInputValue;
			cout << "Enter number: ";
			cin >> userInputValue;
			stack.pushFront(userInputValue);
			queue.pushFront(userInputValue);
		}
		else if (userOptionInput == 2)
		{
			stack.popFront();
			queue.popFront1();
			cout << endl << endl;
			cout << "You have popped the front elements." << endl;

		}
		else if (userOptionInput == 3)
		{
			cout << "Stack elements:" << endl;
			for (int i = 0; i < stack.getSize(); i++)
			{
				cout << stack[i] << endl;
			}
			cout << "Queue elements:" << endl;
			for (int i = queue.getSize()-1; i >= 0; i--)
			{
				cout << queue[i] << endl;
			}
			//unordered.pop();
			//stack.pop();
		}
		int first = 0;
		int last = queue.getSize()-1;
		cout << "Top elements of sets:" << endl;
		cout << "Queue: " << queue[last] << endl;
		cout << "Stack: " << stack[first] << endl;
		system("Pause");
		system("CLS");
	}
	
}